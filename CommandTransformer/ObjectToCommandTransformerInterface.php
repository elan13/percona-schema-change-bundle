<?php


namespace Elan\PerconaSchemaChangeBundle\CommandTransformer;

use Symfony\Component\Process\Process;

/**
 * Interface ObjectToCommandAsArrayTransformerInterface
 *
 * @package Elan\PerconaSchemaChangeBundle\CommandTransformer
 */
interface ObjectToCommandTransformerInterface
{
    /**
     * @return array
     */
    public function getCommandAsArray(): array;

    /**
     * @return Process
     */
    public function createProcess(): Process;
}