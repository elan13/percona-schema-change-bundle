<?php

namespace Elan\PerconaSchemaChangeBundle\CommandTransformer;

use Elan\PerconaSchemaChangeBundle\PerconaSchemaChange;
use Symfony\Component\Process\Process;
use Symfony\Component\Validator\Validation;

/**
 * Class PerconaToCommandTransformer
 *
 * @package Elan\PerconaSchemaChangeBundle\CommandTransformer
 */
class PerconaToCommandTransformer implements ObjectToCommandTransformerInterface
{
    /**
     * @var PerconaSchemaChange
     */
    private $perconaSchemaChange;

    /**
     * PerconaToCommandAsArrayTransformer constructor.
     *
     * @param PerconaSchemaChange $perconaSchemaChange
     */
    public function __construct(PerconaSchemaChange $perconaSchemaChange)
    {
        $this->perconaSchemaChange = $perconaSchemaChange;

        $this->validate();
    }


    /**
     * validate PerconaSchemaChange $this->perconaSchemaChange
     */
    protected function validate(): void
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $violations = $validator->validate($this->perconaSchemaChange);
        if (count($violations) > 0) {
            throw new \LogicException(sprintf('%s configured  incorrectly. %s', PerconaSchemaChange::class, (string) $violations));
        }
    }


    /**
     * @return array
     */
    public function getCommandAsArray():  array
    {
        $propertiesRaw = $this->perconaSchemaChange->__toArray();

        // table option must be t={table_name} without separation
        $tableOption = "t={$propertiesRaw['table']}";
        unset($propertiesRaw['table'], $propertiesRaw['enabled']);

        $command = [PerconaSchemaChange::COMMAND];
        if ($propertiesRaw) {
            foreach ($propertiesRaw as $propertyName => $propertyValue) {
                $command[] = '--'.$this->convertCamelCaseToLowerKebabCase($propertyName);

                // bool options have no value, example: --dry-run
                if (!is_bool($propertyValue)) {
                    $command[] = $propertyValue;
                }
            }
        }
        $command[] = $tableOption;

        return $command;
    }

    /**
     * @return Process
     */
    public function createProcess(): Process
    {
        return new Process($this->getCommandAsArray());
    }

    /**
     * @param string $str
     * @return string
     */
    protected function convertCamelCaseToLowerKebabCase(string $str): string
    {
        return strtolower(preg_replace('/[A-Z]/', '-\\0', lcfirst($str)));
    }
}