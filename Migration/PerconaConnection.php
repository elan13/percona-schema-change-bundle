<?php


namespace Elan\PerconaSchemaChangeBundle\Migration;


use Closure;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Driver\ResultStatement;
use Doctrine\DBAL\SQLParserUtils;
use Doctrine\Migrations\Exception\AbortMigration;
use Doctrine\Migrations\OutputWriter;
use Elan\PerconaSchemaChangeBundle\CommandTransformer\ObjectToCommandTransformerInterface;
use Elan\PerconaSchemaChangeBundle\CommandTransformer\PerconaToCommandTransformer;
use Elan\PerconaSchemaChangeBundle\PerconaSchemaChange;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Process\Process;
use Throwable;

class PerconaConnection extends Connection
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var PerconaSchemaChange
     */
    private $perconaSchemaChange;

    public function __construct(Connection $connection,  PerconaSchemaChange $perconaSchemaChange = null)
    {
        $this->connection = $connection;
        $this->perconaSchemaChange = $perconaSchemaChange;
    }


    /**
     * Executes an, optionally parametrized, SQL query.
     *
     * If the query is parametrized, a prepared statement is used.
     * If an SQLLogger is configured, the execution is logged.
     *
     * @param string                 $query  The SQL query to execute.
     * @param mixed[]                $params The parameters to bind to the query, if any.
     * @param int[]|string[]         $types  The types the previous parameters are in.
     * @param QueryCacheProfile|null $qcp    The query cache profile, optional.
     *
     * @return ResultStatement The executed statement.
     *
     * @throws DBALException
     */
    public function executeQuery($query, array $params = [], $types = [], ?QueryCacheProfile $qcp = null)
    {

        if (false === stripos($query, 'alter') || $this->isPerconaSchemaChangeDisabled()) {
            return $this->connection->executeQuery($query, $params, $types);
        }

        $this->prepareSql($query);
        $this->prepareConnection();

        $process = $this->getCommand()->createProcess();

        $outputStdOut = new StreamOutput(fopen('php://stdout', 'w'));
        $outputStdErr = new StreamOutput(fopen('php://stderr', 'w'));
        $process->run(function ($type, $buffer) use ($outputStdOut, $outputStdErr, $process) {
            if (Process::ERR === $type) {
                $outputStdErr->write($buffer);
            } else {
                $outputStdOut->write($buffer);
            }
        });

        if (false === $process->isSuccessful()) {
            throw new AbortMigration(sprintf('Percona failed with query "%s"', $query));
        }

        return $this->connection->executeQuery('SELECT TRUE', $params, $types);
    }

    /**
     * @return bool
     */
    protected function isPerconaSchemaChangeDisabled()
    {
        return !($this->perconaSchemaChange instanceof PerconaSchemaChange) || !$this->perconaSchemaChange->isEnabled();
    }

    /**
     * extract database, table and sql without sentence 'alter table'
     *
     * @param string $sql
     */
    protected function prepareSql(string $sql)
    {
        preg_match('/alter\s+table\s+([`\w]+\.)?([`\w]+)\s+(.+)/si', $sql, $matches);
        $database = $matches[1] ? $this->trimQuotes(substr($matches[1], 0, -1)) : $this->getDatabase();
        $table = $this->trimQuotes($matches[2]);
        $sql = $matches[3];

        $this->perconaSchemaChange->setAlter($sql);
        $this->perconaSchemaChange->setDatabase($database);
        $this->perconaSchemaChange->setTable($table);
    }

    /**
     * get connection from config file or set connection directly
     */
    protected function prepareConnection()
    {
        if (!$this->perconaSchemaChange->getConfig()) {
            $this->perconaSchemaChange->setConnection(
                $this->connection->getHost(),
                (int) $this->connection->getPort(),
                $this->connection->getUsername(),
                $this->connection->getPassword()
            );
        }
    }

    /**
     * @return ObjectToCommandTransformerInterface
     */
    protected function getCommand(): ObjectToCommandTransformerInterface
    {
        return new PerconaToCommandTransformer($this->perconaSchemaChange);
    }

    /**
     * Trim quotes from the identifier.
     *
     * @param string $identifier
     * @return string
     */
    protected function trimQuotes($identifier)
    {
        return str_replace(['`', '"', '[', ']'], '', $identifier);
    }

    public function getParams()
    {
        return $this->connection->getParams(); 
    }

    public function getDatabase()
    {
        return $this->connection->getDatabase(); 
    }

    public function getHost()
    {
        return $this->connection->getHost(); 
    }

    public function getPort()
    {
        return $this->connection->getPort(); 
    }

    public function getUsername()
    {
        return $this->connection->getUsername(); 
    }

    public function getPassword()
    {
        return $this->connection->getPassword(); 
    }

    public function getDriver()
    {
        return $this->connection->getDriver(); 
    }

    public function getConfiguration()
    {
        return $this->connection->getConfiguration(); 
    }

    public function getEventManager()
    {
        return $this->connection->getEventManager(); 
    }

    public function getDatabasePlatform()
    {
        return $this->connection->getDatabasePlatform(); 
    }

    public function getExpressionBuilder()
    {
        return $this->connection->getExpressionBuilder(); 
    }

    public function connect()
    {
        return $this->connection->connect(); 
    }

    public function isAutoCommit()
    {
        return $this->connection->isAutoCommit(); 
    }

    public function setAutoCommit($autoCommit)
    {
        return $this->connection->setAutoCommit($autoCommit); 
    }

    public function setFetchMode($fetchMode)
    {
        $this->connection->setFetchMode($fetchMode); 
    }

    public function fetchAssoc($statement, array $params = [], array $types = [])
    {
        return $this->connection->fetchAssoc($statement, $params, $types); 
    }

    public function fetchArray($statement, array $params = [], array $types = [])
    {
        return $this->connection->fetchArray($statement, $params, $types); 
    }

    public function fetchColumn($statement, array $params = [], $column = 0, array $types = [])
    {
        return $this->connection->fetchColumn($statement, $params, $column, $types); 
    }

    public function isConnected()
    {
        return $this->connection->isConnected(); 
    }

    public function isTransactionActive()
    {
        return $this->connection->isTransactionActive(); 
    }

    public function delete($tableExpression, array $identifier, array $types = [])
    {
        return $this->connection->delete($tableExpression, $identifier, $types); 
    }

    public function close()
    {
        $this->connection->close(); 
    }

    public function setTransactionIsolation($level)
    {
        return $this->connection->setTransactionIsolation($level); 
    }

    public function getTransactionIsolation()
    {
        return $this->connection->getTransactionIsolation(); 
    }

    public function update($tableExpression, array $data, array $identifier, array $types = [])
    {
        return $this->connection->update($tableExpression, $data, $identifier, $types); 
    }

    public function insert($tableExpression, array $data, array $types = [])
    {
        return $this->connection->insert($tableExpression, $data, $types); 
    }

    public function quoteIdentifier($str)
    {
        return $this->connection->quoteIdentifier($str); 
    }

    public function quote($input, $type = null)
    {
        return $this->connection->quote($input, $type); 
    }

    public function fetchAll($sql, array $params = [], $types = [])
    {
        return $this->connection->fetchAll($sql, $params, $types); 
    }

    public function prepare($statement)
    {
        return $this->connection->prepare($statement); 
    }

    public function executeCacheQuery($query, $params, $types, QueryCacheProfile $qcp)
    {
        return $this->connection->executeCacheQuery($query, $params, $types, $qcp); 
    }

    public function project($query, array $params, Closure $function)
    {
        return $this->connection->project($query, $params, $function); 
    }

    public function query()
    {
        return $this->connection->query(); 
    }

    public function executeUpdate($query, array $params = [], array $types = [])
    {
        return $this->connection->executeUpdate($query, $params, $types); 
    }

    public function exec($statement)
    {
        return $this->connection->exec($statement); 
    }

    public function getTransactionNestingLevel()
    {
        return $this->connection->getTransactionNestingLevel(); 
    }

    public function errorCode()
    {
        return $this->connection->errorCode(); 
    }

    public function errorInfo()
    {
        return $this->connection->errorInfo(); 
    }

    public function lastInsertId($seqName = null)
    {
        return $this->connection->lastInsertId($seqName); 
    }

    public function transactional(Closure $func)
    {
        return $this->connection->transactional($func); 
    }

    public function setNestTransactionsWithSavepoints($nestTransactionsWithSavepoints)
    {
        $this->connection->setNestTransactionsWithSavepoints($nestTransactionsWithSavepoints); 
    }

    public function getNestTransactionsWithSavepoints()
    {
        return $this->connection->getNestTransactionsWithSavepoints(); 
    }

    protected function _getNestedTransactionSavePointName()
    {
        return $this->connection->_getNestedTransactionSavePointName(); 
    }

    public function beginTransaction()
    {
        $this->connection->beginTransaction(); 
    }

    public function commit()
    {
        return $this->connection->commit(); 
    }

    public function rollBack()
    {
        $this->connection->rollBack(); 
    }

    public function createSavepoint($savepoint)
    {
        $this->connection->createSavepoint($savepoint); 
    }

    public function releaseSavepoint($savepoint)
    {
        $this->connection->releaseSavepoint($savepoint);
    }

    public function rollbackSavepoint($savepoint)
    {
        $this->connection->rollbackSavepoint($savepoint); 
    }

    public function getWrappedConnection()
    {
        return $this->connection->getWrappedConnection(); 
    }

    public function getSchemaManager()
    {
        return $this->connection->getSchemaManager(); 
    }

    public function setRollbackOnly()
    {
        $this->connection->setRollbackOnly(); 
    }

    public function isRollbackOnly()
    {
        return $this->connection->isRollbackOnly(); 
    }

    public function convertToDatabaseValue($value, $type)
    {
        return $this->connection->convertToDatabaseValue($value, $type); 
    }

    public function convertToPHPValue($value, $type)
    {
        return $this->connection->convertToPHPValue($value, $type); 
    }

    public function resolveParams(array $params, array $types)
    {
        return $this->connection->resolveParams($params, $types); 
    }

    public function createQueryBuilder()
    {
        return $this->connection->createQueryBuilder(); 
    }

    public function ping()
    {
        return $this->connection->ping(); 
    }


}