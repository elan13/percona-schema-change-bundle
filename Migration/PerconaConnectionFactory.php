<?php


namespace Elan\PerconaSchemaChangeBundle\Migration;


use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Elan\PerconaSchemaChangeBundle\PerconaSchemaChange;

class PerconaConnectionFactory extends ConnectionFactory
{
    protected $connectionFactory;
    /**
     * @var PerconaSchemaChange
     */
    private $perconaSchemaChange;

    public function __construct(ConnectionFactory $connectionFactory, PerconaSchemaChange $perconaSchemaChange = null)
    {
        $this->connectionFactory = $connectionFactory;
        $this->perconaSchemaChange = $perconaSchemaChange;
    }

    public function createConnection(array $params, Configuration $config = null, EventManager $eventManager = null, array $mappingTypes = [])
    {
        return new PerconaConnection($this->connectionFactory->createConnection($params, $config, $eventManager, $mappingTypes), $this->perconaSchemaChange);
    }
}