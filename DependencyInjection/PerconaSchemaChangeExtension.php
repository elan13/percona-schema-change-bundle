<?php

namespace Elan\PerconaSchemaChangeBundle\DependencyInjection;

use Elan\PerconaSchemaChangeBundle\PerconaSchemaChange;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class PerconaSchemaChangeExtension
 *
 * @package Elan\PerconaSchemaChangeBundle\DependencyInjection
 */
class PerconaSchemaChangeExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('percona_schema_change_bundle.percona_schema_change');
        foreach ($config as $key => $value) {
            $definition->addMethodCall('set'.ucfirst($key), [$value]);
        }
    }
}