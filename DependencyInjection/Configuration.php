<?php


namespace Elan\PerconaSchemaChangeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('percona_schema');
        $treeBuilder->getRootNode()
            ->children()
                ->booleanNode('enabled')->defaultValue("%env(PERCONA_ENABLED)%")->end()

                ->scalarNode('config')->end()

                ->enumNode('alterForeignKeysMethod')
                    ->values(["auto", "rebuild_constraints", "drop_swap", "none"])
                    ->defaultValue('auto')
                ->end()
                ->booleanNode('noanalyzeBeforeSwap')->end()
                ->scalarNode('charset')->end()
                ->booleanNode('nocheckAlter')->end()
                ->integerNode('checkInterval')->end()
                ->booleanNode('nocheckPlan')->end()
                ->booleanNode('nocheckReplicationFilters')->end()
                ->scalarNode('checkSlaveLag')->end()
                ->scalarNode('chunkIndex')->end()
                ->integerNode('chunkIndexColumns')->end()
                ->integerNode('chunkSize')->end()
                ->floatNode('chunkSizeLimit')->end()
                ->floatNode('chunkTime')->end()
                ->scalarNode('criticalLoad')->end()
                ->booleanNode('defaultEngine')->end()
                ->booleanNode('nodropNewTable')->end()
                ->booleanNode('nodropOldTable')->end()
                ->booleanNode('nodropTrigers')->end()
                ->booleanNode('nocheckUniqueKeyChange')->end()
                ->booleanNode('forceConcatEnums')->end()
                ->floatNode('maxFlowCtl')->end()
                ->integerNode('maxLag')->end()
                ->scalarNode('maxLoad')->end()
                ->booleanNode('preserveTriggers')->end()
                ->scalarNode('newTableName')->end()
                ->booleanNode('nullToNotNull')->end()
                ->scalarNode('pauseFile')->end()
                ->scalarNode('pid')->end()
                ->integerNode('recurse')->end()
                ->enumNode('recursionMethod')
                    ->values(['processlist', 'hosts', 'dsn', 'none'])
                ->end()
                ->scalarNode('skipCheckSlaveLag')->end()
                ->scalarNode('setVars')->end()
                ->floatNode('sleep')->end()
                ->scalarNode('socket')->end()
                ->booleanNode('statistics')->end()
                ->booleanNode('noswapTables')->end()
                ->scalarNode('tries')->end()

                ->scalarNode('progress')->end()
                ->booleanNode('quiet')->end()

                ->booleanNode('print')->end()

                ->booleanNode('dryRun')->end()
                ->booleanNode('execute')->defaultTrue()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}