<?php

namespace Elan\PerconaSchemaChangeBundle\Tests\DependencyInjection;


use Elan\PerconaSchemaChangeBundle\DependencyInjection\PerconaSchemaChangeExtension;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use PHPUnit\Framework\TestCase;

class PerconaSchemaChangeExtensionTest extends TestCase
{
    public function testCorrectOptions() : void
    {
        $container = $this->getContainer();
        $extension = new PerconaSchemaChangeExtension();

        $config = ['execute' => false, 'dryRun' => true];
        $extension->load(['percona_schema_change' => $config], $container);

        $definition = $container->getDefinition('percona_schema_change_bundle.percona_schema_change');

        $this->assertArraySubset([
            ['setExecute', [false]],
            ['setDryRun', [true]],
        ], $definition->getMethodCalls());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessageRegExp  /Unrecognized option "wrongOption" under "percona_schema"/
     */
    public function testNotAllowUnrecognizedOption() : void
    {
        $container = $this->getContainer();
        $extension = new PerconaSchemaChangeExtension();

        $config = ['wrongOption' => false];
        $extension->load(['percona_schema_change' => $config], $container);
    }

    private function getContainer() : ContainerBuilder
    {
        return new ContainerBuilder(new ParameterBag([
            'kernel.debug' => false,
            'kernel.bundles' => [],
            'kernel.cache_dir' => sys_get_temp_dir(),
            'kernel.environment' => 'test',
            'kernel.root_dir' => __DIR__ . '/../../', // src dir
        ]));
    }
}