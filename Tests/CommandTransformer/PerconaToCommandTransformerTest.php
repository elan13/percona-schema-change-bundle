<?php

namespace Elan\PerconaSchemaChangeBundle\Tests\CommandTransformer;

use Elan\PerconaSchemaChangeBundle\CommandTransformer\PerconaToCommandTransformer;
use Elan\PerconaSchemaChangeBundle\PerconaSchemaChange;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class PerconaToCommandTransformerTest extends TestCase
{
    /**
     * @var PerconaSchemaChange
     */
    protected $perconaSchemaChange;

    protected function setUp()
    {
        $perconaSchemaChange = new PerconaSchemaChange();
        $perconaSchemaChange->setConnection('127.0.0.1', 3306, 'user', 'password'); // fake connection
        $perconaSchemaChange->setAlter('CHANGE test_column');
        $perconaSchemaChange->setDatabase('database_test');
        $perconaSchemaChange->setTable('table_test');
        $perconaSchemaChange->setExecute(true);

        $this->perconaSchemaChange = $perconaSchemaChange;
    }

    public function testTransformingObjectToCommand()
    {
        $commandTransformer = new PerconaToCommandTransformer($this->perconaSchemaChange);

        $this->assertEquals([
            'pt-online-schema-change',
            '--alter',
            'CHANGE test_column',
            '--database',
            'database_test',
            '--host',
            '127.0.0.1',
            '--port',
            3306,
            '--user',
            'user',
            '--password',
            'password',
            '--alter-foreign-keys-method',
            'auto',
            '--execute',
            't=table_test',
        ], $commandTransformer->getCommandAsArray());
    }

    public function testIfProcessCommandLineHaveCorrectCommand()
    {
        $commandTransformer = new PerconaToCommandTransformer($this->perconaSchemaChange);

        $commandLine = $commandTransformer->createProcess()->getCommandLine();

        $this->assertContains('pt-online-schema-change', $commandLine);
        $this->assertContains("'--database' 'database_test'", $commandLine);
        $this->assertContains("'t=table_test'", $commandLine);
        $this->assertContains("'--execute'", $commandLine);
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessageRegExp /database:\s+This value should not be blank/
     * @expectedExceptionMessageRegExp /table:\s+This value should not be blank/
     * @expectedExceptionMessageRegExp /alter:\s+This value should not be blank/
     */
    public function testNotAllowToSkipRequiredProperties()
    {
        $this->perconaSchemaChange->setDatabase('');
        $this->perconaSchemaChange->setTable('');
        $this->perconaSchemaChange->setAlter('');

        new PerconaToCommandTransformer($this->perconaSchemaChange);
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessageRegExp /alter:\s+This value is not valid/
     */
    public function testNotAllowSentenceAlterTableInAlterProperty()
    {
        $this->perconaSchemaChange->setAlter('ALTER table_test CHANGE test_column');

        new PerconaToCommandTransformer($this->perconaSchemaChange);
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessageRegExp /--dry-run and --execute are mutually exclusive/
     */
    public function testNotAllowDryRunAndExecuteBeEnabledTogether()
    {
        $this->perconaSchemaChange->setDryRun(true);
        $this->perconaSchemaChange->setExecute(true);

        new PerconaToCommandTransformer($this->perconaSchemaChange);
    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessageRegExp /--dry-run and --execute are mutually exclusive/
     */
    public function testNotAllowDryRunAndExecuteBeDisabledTogether()
    {
        $this->perconaSchemaChange->setDryRun(false);
        $this->perconaSchemaChange->setExecute(false);

        new PerconaToCommandTransformer($this->perconaSchemaChange);
    }
}