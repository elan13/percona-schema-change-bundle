<?php

namespace Elan\PerconaSchemaChangeBundle;

use Elan\PerconaSchemaChangeBundle\CommandTransformer\OptionNameConverterInterface;
use Elan\PerconaSchemaChangeBundle\CommandTransformer\PropertiesToOptionsConvertableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Read more information:
 * https://www.percona.com/doc/percona-toolkit/{current_version}/pt-online-schema-change.html
 *
 * pt-online-schema-change alters a table’s structure without blocking reads or writes.
 * Example: pt-online-schema-change --alter "ADD COLUMN c1 INT" D=database_test,t=table_test
 */
class PerconaSchemaChange
{
    const COMMAND = 'pt-online-schema-change';

    /**
     * Not an option. It can be used to specify if PerconaSchemaChange is activated
     *
     * @var bool
     */
    private $enabled  = true;

    /**
     * Comma-separated list of config files,
     * This must be the first option on the command line
     * You must specify the option and the path to the file separated by whitespace without an equal sign between them
     *
     * Example: --config /path/to/file
     * file content example: D=test,t=t1,h=127.0.0.1,u=root,P=13010
     *
     * @Assert\Type("string")
     */
    private $config;

    /**
     * The schema modification, without the ALTER TABLE keywords.
     * Example: --alter "ADD COLUMN c1 INT"
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Regex(
     *     pattern="/alter\s+table/i",
     *     match=false
     * )
     */
    private $alter;

    /**
     * Table name. Must be specified separately from --alter scope
     *
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $table;




    /** START connection --------------------------------------------------------------- */

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $database;

    /**
     * @Assert\Type("string")
     */
    private $host;

    /**
     * @Assert\Type("int")
     */
    private $port;

    /**
     * @Assert\Type("string")
     */
    private $user;

    /**
     * @Assert\Type("string")
     */
    private $password;

    /**
     * @Assert\Type("string")
     */
    private $slaveUser;

    /**
     * @Assert\Type("string")
     */
    private $slavePassword;


    /** END connection --------------------------------------------------------------- */


    /**
     * Default character set
     *
     * @Assert\Type("string")
     */
    private $charset;

    /**
     * Socket file to use for connection.
     *
     * @Assert\Type("string")
     */
    private $socket;

    /**
     * Remove ENGINE from the new table.
     *
     * @Assert\Type("bool")
     */
    private $defaultEngine = false;




    /**
     * 4 approaches how to alter foreign keys.
     * Read for more information
     * https://www.percona.com/doc/percona-toolkit/{current_version}/pt-online-schema-change.html#cmdoption-pt-online-schema-change-alter-foreign-keys-method
     *
     * @Assert\Choice({"auto", "rebuild_constraints", "drop_swap", "none"})
     */
    private $alterForeignKeysMethod = 'auto';

    /**
     * Sleep time between checks for --max-lag.
     * Default: 1
     *
     * @Assert\Type("int")
     */
    private $checkInterval;

    /**
     * Pause the data copy until this replica’s lag is less than --max-lag.
     * The value is a DSN that inherits properties from the the connection options (--port, --user, etc.).
     * This option overrides normal behavior of finding and monitoring replication lag on ALL connected replicas
     *
     * @Assert\Type("string")
     */
    private $checkSlaveLag;

    /**
     * Prefer this index for chunking tables. By default, the tool chooses the most appropriate index for chunking.
     *
     * @Assert\Type("string")
     */
    private $chunkIndex;

    /**
     * Use only this many left-most columns of a --chunk-index
     *
     * @Assert\Type("int")
     */
    private $chunkIndexColumns;

    /**
     * Number of rows to select for each chunk copied
     * Default: 1000
     *
     * @Assert\Type("int")
     */
    private $chunkSize;

    /**
     * Do not copy chunks this much larger than the desired chunk size.
     * Default: 4.0
     *
     * @Assert\Type("float")
     */
    private $chunkSizeLimit;

    /**
     * Adjust the chunk size dynamically so each data-copy query takes this long to execute.
     * Default: 0.5
     *
     * @Assert\Type("float")
     */
    private $chunkTime;

    /**
     * Examine SHOW GLOBAL STATUS after every chunk, and abort if the load is too high
     * Default: Threads_running=50
     *
     * @Assert\Type("string")
     */
    private $criticalLoad;

    /**
     * This options bypasses confirmation in case of using alter-foreign-keys-method = none,
     * which might break foreign key constraints.
     *
     * @Assert\Type("bool")
     */
    private $forceConcatEnums = false;

    /**
     * Check average time cluster spent pausing for Flow Control and make tool pause if it goes over the percentage
     * indicated in the option
     *
     * @Assert\Type("float")
     */
    private $maxFlowCtl;

    /**
     * Pause the data copy until all replicas’ lag is less than this value.
     * Default: 1 (seconds)
     *
     * @Assert\Type("int")
     */
    private $maxLag;

    /**
     * Examine SHOW GLOBAL STATUS after every chunk, and pause if any status variables are higher than their thresholds.
     * Default: Threads_running=25
     *
     * @Assert\Type("string")
     */
    private $maxLoad;

    /**
     * This allows us to add the triggers needed even if the table already has its own triggers.
     * If this option is enabled, it will try to copy all the existing triggers to the new table BEFORE start
     * copying rows from the original table to ensure the old triggers can be applied after altering the table.
     *
     * @Assert\Type("bool")
     */
    private $preserveTriggers = false;

    /**
     * New table name before it is swapped. %T is replaced with the original table name.
     * Default: %T_new
     *
     * @Assert\Type("string")
     * @Assert\Regex("/%T/i")
     */
    private $newTableName;

    /**
     * Allows MODIFYing a column that allows NULL values to one that doesn't allow them.
     * The rows which contain NULL values will be converted to the defined default value.
     * If no explicit DEFAULT value is given MySQL will assign a default value based on datatype,
     * e.g. 0 for number datatypes, ‘’ for string datatypes.
     *
     * @Assert\Type("bool")
     */
    private $nullToNotNull = false;

    /**
     * Execution will be paused while the file specified by this param exists.
     *
     * @Assert\Type("string")
     */
    private $pauseFile;

    /**
     * Create the given PID file.
     * The tool won’t start if the PID file already exists and the PID it contains is different than the current PID.
     *
     * @Assert\Type("string")
     */
    private $pid;

    /**
     * Number of levels to recurse in the hierarchy when discovering replicas
     * Default: infinite
     *
     * @Assert\Type("int")
     */
    private $recurse;

    /**
     * Preferred recursion method for discovering replicas.
     * Default: processlist,hosts
     *
     * @Assert\Choice({"processlist", "hosts", "dsn'", "none"})
     */
    private $recursionMethod;

    /**
     * DSN to skip when checking slave lag
     * Example: –skip-check-slave-lag h=127.0.0.1,P=12345 –skip-check-slave-lag h=127.0.0.1,P=12346
     * type: DSN; repeatable: yes
     *
     * @Assert\Type("string")
     */
    private $skipCheckSlaveLag;

    /**
     * Set the MySQL variables in this comma-separated list of variable=value pairs.
     * Example: --set-vars sql_mode=\'STRICT_ALL_TABLES\\,ALLOW_INVALID_DATES\'
     *
     * @Assert\Type("string")
     */
    private $setVars;

    /**
     * How long to sleep (in seconds) after copying each chunk.
     * A small, sub-second value should be used, like 0.1, else the tool could take a long time to copy large tables.
     * Default: 0 (seconds)
     *
     * @Assert\Type("float")
     */
    private $sleep;

    /**
     * How many times to try critical operations.
     * If certain operations fail due to non-fatal, recoverable errors, the tool waits and tries the operation again.
     * Example: --tries create_triggers:5:0.5,drop_triggers:5:0.5.
     * That makes the tool try create_triggers and drop_triggers 5 times with a 0.5 second wait between tries
     *
     * @Assert\Type("string")
     */
    private $tries;




    /**
     * DISABLE - Execute analyze table on the new table before swapping with the old one.
     * Default: yes (analyzed)
     *
     * @Assert\Type("bool")
     */
    private $noanalyzeBeforeSwap = false;

    /**
     * DISABLE - Parses the --alter specified and tries to warn of possible unintended behavior.
     * Default: yes (parsed)
     *
     * @Assert\Type("bool")
     */
    private $nocheckAlter = false;

    /**
     * DISABLE - Check query execution plans for safety.
     * Default: yes (checked)
     *
     * @Assert\Type("bool")
     */
    private $nocheckPlan = false;

    /**
     * DISABLE - Abort if any replication filter is set on any server
     * Default: yes (aborted)
     *
     * @Assert\Type("bool")
     */
    private $nocheckReplicationFilters = false;

    /**
     * Default: yes (dropped)
     *
     * @Assert\Type("bool")
     */
    private $nodropNewTable = false;

    /**
     * Default: yes (dropped)
     *
     * @Assert\Type("bool")
     */
    private $nodropOldTable = false;


    /**
     * DISABLE - drop triggers on the old table
     * Default: yes (dropped)
     *
     * @Assert\Type("bool")
     */
    private $nodropTrigers = false;

    /**
     * DISABLE check - Avoid running if the statement is trying to add an unique index.
     * Since the command uses INSERT IGNORE to copy rows to the new table, if the row being written produces
     * a duplicate key, it will fail silently and data will be lost.
     * Default: yes (checked)
     *
     * @Assert\Type("bool")
     */
    private $nocheckUniqueKeyChange = false;

    /**
     * DISABLE - Swap the original table and the new, altered table.
     * Default: yes (swapped)
     *
     * @Assert\Type("bool")
     */
    private $noswapTables = false;




    /**
     * Print statistics about internal counters.
     * This is useful to see how many warnings were suppressed compared to the number of INSERT.
     *
     * @Assert\Type("bool")
     */
    private $statistics = false;

    /**
     * Print progress reports to STDERR while copying rows. The value is a comma-separated list with two parts.
     * Default: time,30
     *
     * @Assert\Type("string")
     */
    private $progress;

    /**
     * Do not print messages to STDOUT (disables --progress). Errors and warnings are still printed to STDERR.
     *
     * @Assert\Type("bool")
     */
    private $quite = false;

    /**
     * Print SQL statements to STDOUT. It can be used with --dry-run, for example
     *
     * @Assert\Type("bool")
     */
    private $print = false;




    /**
     * --dry-run and --execute are mutually exclusive.
     * Create and alter the new table, but do not create triggers, copy data, or replace the original table.
     *
     * @Assert\Type("bool")
     */
    private $dryRun = false;

    /**
     * This option is required to alter the table
     *
     * @Assert\Type("bool")
     * @Assert\Expression(
     *     "this.getDryRun() != value",
     *     message="--dry-run and --execute are mutually exclusive"
     * )
     */
    private $execute = false;



    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     * @return $this
     */
    public function setConnection($host = '127.0.0.1', $port = 3306, $user = 'root', $password = '')
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param string $config
     * @return PerconaSchemaChange
     */
    public function setConfig(string $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlter()
    {
        return $this->alter;
    }

    /**
     * @param mixed $alter
     * @return PerconaSchemaChange
     */
    public function setAlter(string $alter)
    {
        $this->alter = $alter;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     * @return PerconaSchemaChange
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param string $database
     * @return PerconaSchemaChange
     */
    public function setDatabase(string $database)
    {
        $this->database = $database;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host): void
    {
        $this->host = $host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port): void
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return PerconaSchemaChange
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlaveUser()
    {
        return $this->slaveUser;
    }

    /**
     * @param string $slaveUser
     * @return PerconaSchemaChange
     */
    public function setSlaveUser(string $slaveUser)
    {
        $this->slaveUser = $slaveUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlavePassword()
    {
        return $this->slavePassword;
    }

    /**
     * @param string $slavePassword
     * @return PerconaSchemaChange
     */
    public function setSlavePassword(string $slavePassword)
    {
        $this->slavePassword = $slavePassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     * @return PerconaSchemaChange
     */
    public function setCharset(string $charset)
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * @param string $socket
     * @return PerconaSchemaChange
     */
    public function setSocket(string $socket)
    {
        $this->socket = $socket;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDefaultEngine()
    {
        return $this->defaultEngine;
    }

    /**
     * @param bool $defaultEngine
     * @return PerconaSchemaChange
     */
    public function setDefaultEngine(bool $defaultEngine)
    {
        $this->defaultEngine = $defaultEngine;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlterForeignKeysMethod()
    {
        return $this->alterForeignKeysMethod;
    }

    /**
     * @param string $alterForeignKeysMethod
     * @return PerconaSchemaChange
     */
    public function setAlterForeignKeysMethod(string $alterForeignKeysMethod)
    {
        $this->alterForeignKeysMethod = $alterForeignKeysMethod;
        return $this;
    }

    /**
     * @return int
     */
    public function getCheckInterval()
    {
        return $this->checkInterval;
    }

    /**
     * @param int $checkInterval
     * @return PerconaSchemaChange
     */
    public function setCheckInterval(int $checkInterval)
    {
        $this->checkInterval = $checkInterval;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckSlaveLag()
    {
        return $this->checkSlaveLag;
    }

    /**
     * @param string $checkSlaveLag
     * @return PerconaSchemaChange
     */
    public function setCheckSlaveLag(string $checkSlaveLag)
    {
        $this->checkSlaveLag = $checkSlaveLag;
        return $this;
    }

    /**
     * @return string
     */
    public function getChunkIndex()
    {
        return $this->chunkIndex;
    }

    /**
     * @param string $chunkIndex
     * @return PerconaSchemaChange
     */
    public function setChunkIndex(string $chunkIndex)
    {
        $this->chunkIndex = $chunkIndex;
        return $this;
    }

    /**
     * @return int
     */
    public function getChunkIndexColumns()
    {
        return $this->chunkIndexColumns;
    }

    /**
     * @param int $chunkIndexColumns
     * @return PerconaSchemaChange
     */
    public function setChunkIndexColumns(int $chunkIndexColumns)
    {
        $this->chunkIndexColumns = $chunkIndexColumns;
        return $this;
    }

    /**
     * @return int
     */
    public function getChunkSize()
    {
        return $this->chunkSize;
    }

    /**
     * @param int $chunkSize
     * @return PerconaSchemaChange
     */
    public function setChunkSize(int $chunkSize)
    {
        $this->chunkSize = $chunkSize;
        return $this;
    }

    /**
     * @return float
     */
    public function getChunkSizeLimit()
    {
        return $this->chunkSizeLimit;
    }

    /**
     * @param float $chunkSizeLimit
     * @return PerconaSchemaChange
     */
    public function setChunkSizeLimit(float $chunkSizeLimit)
    {
        $this->chunkSizeLimit = $chunkSizeLimit;
        return $this;
    }

    /**
     * @return float
     */
    public function getChunkTime()
    {
        return $this->chunkTime;
    }

    /**
     * @param float $chunkTime
     * @return PerconaSchemaChange
     */
    public function setChunkTime(float $chunkTime)
    {
        $this->chunkTime = $chunkTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getCriticalLoad()
    {
        return $this->criticalLoad;
    }

    /**
     * @param string $criticalLoad
     * @return PerconaSchemaChange
     */
    public function setCriticalLoad(string $criticalLoad)
    {
        $this->criticalLoad = $criticalLoad;
        return $this;
    }

    /**
     * @return bool
     */
    public function getForceConcatEnums()
    {
        return $this->forceConcatEnums;
    }

    /**
     * @param bool $forceConcatEnums
     * @return PerconaSchemaChange
     */
    public function setForceConcatEnums(bool $forceConcatEnums)
    {
        $this->forceConcatEnums = $forceConcatEnums;
        return $this;
    }

    /**
     * @return float
     */
    public function getMaxFlowCtl()
    {
        return $this->maxFlowCtl;
    }

    /**
     * @param float $maxFlowCtl
     * @return PerconaSchemaChange
     */
    public function setMaxFlowCtl(float $maxFlowCtl)
    {
        $this->maxFlowCtl = $maxFlowCtl;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxLag()
    {
        return $this->maxLag;
    }

    /**
     * @param int $maxLag
     * @return PerconaSchemaChange
     */
    public function setMaxLag(int $maxLag)
    {
        $this->maxLag = $maxLag;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaxLoad()
    {
        return $this->maxLoad;
    }

    /**
     * @param string $maxLoad
     * @return PerconaSchemaChange
     */
    public function setMaxLoad(string $maxLoad)
    {
        $this->maxLoad = $maxLoad;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPreserveTriggers()
    {
        return $this->preserveTriggers;
    }

    /**
     * @param bool $preserveTriggers
     * @return PerconaSchemaChange
     */
    public function setPreserveTriggers(bool $preserveTriggers)
    {
        $this->preserveTriggers = $preserveTriggers;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewTableName()
    {
        return $this->newTableName;
    }

    /**
     * @param string $newTableName
     * @return PerconaSchemaChange
     */
    public function setNewTableName(string $newTableName)
    {
        $this->newTableName = $newTableName;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNullToNotNull()
    {
        return $this->nullToNotNull;
    }

    /**
     * @param bool $nullToNotNull
     * @return PerconaSchemaChange
     */
    public function setNullToNotNull(bool $nullToNotNull)
    {
        $this->nullToNotNull = $nullToNotNull;
        return $this;
    }

    /**
     * @return string
     */
    public function getPauseFile()
    {
        return $this->pauseFile;
    }

    /**
     * @param string $pauseFile
     * @return PerconaSchemaChange
     */
    public function setPauseFile(string $pauseFile)
    {
        $this->pauseFile = $pauseFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param string $pid
     * @return PerconaSchemaChange
     */
    public function setPid(string $pid)
    {
        $this->pid = $pid;
        return $this;
    }

    /**
     * @return int
     */
    public function getRecurse()
    {
        return $this->recurse;
    }

    /**
     * @param int $recurse
     * @return PerconaSchemaChange
     */
    public function setRecurse(int $recurse)
    {
        $this->recurse = $recurse;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecursionMethod()
    {
        return $this->recursionMethod;
    }

    /**
     * @param string $recursionMethod
     * @return PerconaSchemaChange
     */
    public function setRecursionMethod(string $recursionMethod)
    {
        $this->recursionMethod = $recursionMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getSkipCheckSlaveLag()
    {
        return $this->skipCheckSlaveLag;
    }

    /**
     * @param string $skipCheckSlaveLag
     * @return PerconaSchemaChange
     */
    public function setSkipCheckSlaveLag(string $skipCheckSlaveLag)
    {
        $this->skipCheckSlaveLag = $skipCheckSlaveLag;
        return $this;
    }

    /**
     * @return string
     */
    public function getSetVars()
    {
        return $this->setVars;
    }

    /**
     * @param string $setVars
     * @return PerconaSchemaChange
     */
    public function setSetVars(string $setVars)
    {
        $this->setVars = $setVars;
        return $this;
    }

    /**
     * @return float
     */
    public function getSleep()
    {
        return $this->sleep;
    }

    /**
     * @param float $sleep
     * @return PerconaSchemaChange
     */
    public function setSleep(float $sleep)
    {
        $this->sleep = $sleep;
        return $this;
    }

    /**
     * @return string
     */
    public function getTries()
    {
        return $this->tries;
    }

    /**
     * @param string $tries
     * @return PerconaSchemaChange
     */
    public function setTries(string $tries)
    {
        $this->tries = $tries;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNoanalyzeBeforeSwap()
    {
        return $this->noanalyzeBeforeSwap;
    }

    /**
     * @param bool $noanalyzeBeforeSwap
     * @return PerconaSchemaChange
     */
    public function setNoanalyzeBeforeSwap(bool $noanalyzeBeforeSwap)
    {
        $this->noanalyzeBeforeSwap = $noanalyzeBeforeSwap;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNocheckAlter()
    {
        return $this->nocheckAlter;
    }

    /**
     * @param bool $nocheckAlter
     * @return PerconaSchemaChange
     */
    public function setNocheckAlter(bool $nocheckAlter)
    {
        $this->nocheckAlter = $nocheckAlter;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNocheckPlan()
    {
        return $this->nocheckPlan;
    }

    /**
     * @param bool $nocheckPlan
     * @return PerconaSchemaChange
     */
    public function setNocheckPlan(bool $nocheckPlan)
    {
        $this->nocheckPlan = $nocheckPlan;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNocheckReplicationFilters()
    {
        return $this->nocheckReplicationFilters;
    }

    /**
     * @param bool $nocheckReplicationFilters
     * @return PerconaSchemaChange
     */
    public function setNocheckReplicationFilters(bool $nocheckReplicationFilters)
    {
        $this->nocheckReplicationFilters = $nocheckReplicationFilters;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNodropNewTable()
    {
        return $this->nodropNewTable;
    }

    /**
     * @param bool $nodropNewTable
     * @return PerconaSchemaChange
     */
    public function setNodropNewTable(bool $nodropNewTable)
    {
        $this->nodropNewTable = $nodropNewTable;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNodropOldTable()
    {
        return $this->nodropOldTable;
    }

    /**
     * @param bool $nodropOldTable
     * @return PerconaSchemaChange
     */
    public function setNodropOldTable(bool $nodropOldTable)
    {
        $this->nodropOldTable = $nodropOldTable;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNodropTrigers()
    {
        return $this->nodropTrigers;
    }

    /**
     * @param bool $nodropTrigers
     * @return PerconaSchemaChange
     */
    public function setNodropTrigers(bool $nodropTrigers)
    {
        $this->nodropTrigers = $nodropTrigers;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNocheckUniqueKeyChange()
    {
        return $this->nocheckUniqueKeyChange;
    }

    /**
     * @param bool $nocheckUniqueKeyChange
     * @return PerconaSchemaChange
     */
    public function setNocheckUniqueKeyChange(bool $nocheckUniqueKeyChange)
    {
        $this->nocheckUniqueKeyChange = $nocheckUniqueKeyChange;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNoswapTables()
    {
        return $this->noswapTables;
    }

    /**
     * @param bool $noswapTables
     * @return PerconaSchemaChange
     */
    public function setNoswapTables(bool $noswapTables)
    {
        $this->noswapTables = $noswapTables;
        return $this;
    }



    
    /**
     * @return bool
     */
    public function getStatistics()
    {
        return $this->statistics;
    }

    /**
     * @param bool $statistics
     * @return PerconaSchemaChange
     */
    public function setStatistics(bool $statistics)
    {
        $this->statistics = $statistics;
        return $this;
    }

    /**
     * @return string
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param string $progress
     * @return PerconaSchemaChange
     */
    public function setProgress(string $progress)
    {
        $this->progress = $progress;
        return $this;
    }

    /**
     * @return bool
     */
    public function getQuite()
    {
        return $this->quite;
    }

    /**
     * @param bool $quite
     * @return PerconaSchemaChange
     */
    public function setQuite(bool $quite)
    {
        $this->quite = $quite;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPrint()
    {
        return $this->print;
    }

    /**
     * @param bool $print
     * @return PerconaSchemaChange
     */
    public function setPrint(bool $print)
    {
        $this->print = $print;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDryRun()
    {
        return $this->dryRun;
    }

    /**
     * @param bool $dryRun
     * @return PerconaSchemaChange
     */
    public function setDryRun(bool $dryRun = true)
    {
        $this->dryRun = $dryRun;

        return $this;
    }

    /**
     * @return bool
     */
    public function getExecute()
    {
        return $this->execute;
    }

    /**
     * @param bool $execute
     * @return PerconaSchemaChange
     */
    public function setExecute(bool $execute = true)
    {
        $this->execute = $execute;

        return $this;
    }

    /**
     * return all properties as array
     *
     * @return mixed
     */
    public function __toArray()
    {
        return array_filter(call_user_func('get_object_vars', $this));
    }
}
